from django.shortcuts import render

# Create your views here.
def index(request):
    response = {}
    return render(request, 'index.html', response)

def registration(request):
    response = {}
    return render(request, 'registration.html', response)
