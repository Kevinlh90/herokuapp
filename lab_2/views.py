from django.shortcuts import render

def index(request):
    response = {}
    return render(request, 'index_lab2.html', response)
